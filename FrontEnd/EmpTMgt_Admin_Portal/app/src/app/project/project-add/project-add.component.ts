import { Component, OnInit } from '@angular/core';
import { ProjectService } from '../project.service';
import { ToastrService } from 'ngx-toastr';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Project } from '../project';

@Component({
  selector: 'app-project-add',
  templateUrl: './project-add.component.html',
  styleUrls: ['./project-add.component.css']
})
export class ProjectAddComponent implements OnInit {

  project: Project = new Project();
  projectName=''
  projectDesc=''
  startDate=''
  endDate=''
  status=''

  firstName = ''
  lastName = ''
  email = ''
  address = ''
  birthDate = ''
  contInfo =''
  gender =''
  department =''
  designation =''

  
  customerName=''
  requirements=''

  constructor(
    private modal: NgbActiveModal,
    private projectService: ProjectService,
    private toastr: ToastrService,
  ) { }

  ngOnInit(): void {
  }

  onAdd() {
    if (this.projectName.length == 0) {
      this.toastr.warning('please enter project name')
    } else if (this.projectDesc.length == 0) {
      this.toastr.warning('please enter description')
    } else if (this.startDate.length == 0) {
      this.toastr.warning('please enter start date')
    } else if (this.endDate.length == 0) {
      this.toastr.warning('please enter end date')
    }  else if(this.firstName.length == 0) {
      this.toastr.warning('please enter first name')
    } else if (this.lastName.length == 0) {
      this.toastr.warning('please enter first name')
    } else if (this.email.length == 0) {
      this.toastr.warning('please enter email')
    } else if (this.address.length == 0) {
      this.toastr.warning('please enter address')
    } else if (this.birthDate.length == 0) {
      this.toastr.warning('please enter birthdeleteEmployee date')
    } else if (this.contInfo.length == 0) {
      this.toastr.warning('please enter contact details')
    } else if (this.gender.length == 0) {
      this.toastr.warning('please enter gender')
    }  else if (this.department.length == 0) {
      this.toastr.warning('please enter department')
    } else if (this.designation.length == 0) {
      this.toastr.warning('please enter designation')
    }    
    else {
      this.projectService
        .addProject(this.projectName, this.projectDesc,  this.startDate, this.endDate,this.status,
          this.firstName, this.lastName, this.email, this.address, this.birthDate, 
          this.contInfo ,this.gender ,this.department ,this.designation,this.customerName,this.requirements)
        .subscribe(data => {
          console.log(data)
          this.modal.dismiss('ok')
        }, error => {
          console.log(error)
          this.toastr.error('failed to add !')
        });
    }
  }

  onCancel() {
    this.modal.dismiss('cancel')
  }
}




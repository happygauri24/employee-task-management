import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ProjectService } from '../project.service';
import { Observable } from 'rxjs';
import { Project } from '../project';
import { ProjectAddComponent } from '../project-add/project-add.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.css']
})
export class ProjectListComponent implements OnInit {

    // id: number;
    // projectName: string;
    // projectDesc: string;
    // startDate: string;
    // endDate:string;
    // status:string

  //projects: Observable<Project[]>;
  project:any
  singleProject:any
  

  constructor(
    private modalService: NgbModal,
    private toastr: ToastrService,
    private service: ProjectService,
    private router: Router) { }

  ngOnInit(): void {
    this.fetchProjectList();
  }

  fetchProjectList() {
    this.service.getProjectsList().subscribe(res =>{
      this.project=res;
    //  alert(this.project);
      //console.log(this.project);
    })
   
  }

  onAdd() {
    const modalRef = this.modalService.open(ProjectAddComponent, {size: 'lg'})
    modalRef.result.finally(() => {
      this.fetchProjectList()
    })
  }

  onEdit(id:number){
    this.router.navigate(['/single-project',id]);
  }
}






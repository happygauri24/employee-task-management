export class Project {
    id: number;
    name: string;
    projectDesc: string;
    startDate: string;
    endDate:string;
    status:string;
}
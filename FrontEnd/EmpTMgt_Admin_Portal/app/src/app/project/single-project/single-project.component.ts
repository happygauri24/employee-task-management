import { ProjectService } from './../project.service';
import { Project } from './../project';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-single-project',
  templateUrl: './single-project.component.html',
  styleUrls: ['./single-project.component.css']
})
export class SingleProjectComponent implements OnInit {

  singleProject:any
  id:number
  
  constructor(private route: ActivatedRoute, private service:ProjectService) { }

  ngOnInit(): void {
    this.singleProject=new Project();
    this.id=this.route.snapshot.params['id'];
      this.service.getSingleProjectById(this.id).subscribe(res =>{
        this.singleProject=res;
      });
    }

  }


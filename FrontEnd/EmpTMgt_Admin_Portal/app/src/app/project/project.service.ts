import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class ProjectService {

  private url = 'http://localhost:8081/project'

  constructor(private http: HttpClient) { }

  addProject(projectName: string, projectDesc: string, startDate: string, endDate: string,status: string,
    firstName : string, lastName : string, email : string , address : string , birthDate :string, contInfo :string ,gender :string ,
    department : string ,designation :string,  customerName :string,requirements:string) {
    const body = {
    "projectName": projectName,
    "projectDesc": projectDesc,
    "startDate": startDate,
    "endDate":endDate,
    "status":status,

    "employees":
    
      [{
        "firstName":firstName,
        "lastName":lastName,
        "email":email,
        "address":address,
        "birthDate":birthDate,
        "contactInfo":contInfo,
        "gender":gender,
        "department":department,
        "designation":designation
        }],
        "customerDetails":
        {
            "customerName":customerName,
            "requirements":requirements
        }  
    }

    const httpOptions = {
      headers: new HttpHeaders({
        jwt: sessionStorage['token']
      })
    }
    
    return this.http.post(this.url+'/proj/add-project', body)
  }

  getProjectsList(){
    const httpOptions = {
      headers: new HttpHeaders({
        jwt: sessionStorage['token']
      })
    }
    
    return this.http.get(this.url+'/admin/show-projects');
  }

  getSingleProjectById(id:number){
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    }
    return this.http.get(this.url+'/admin/select-project/'+id);
  }

}

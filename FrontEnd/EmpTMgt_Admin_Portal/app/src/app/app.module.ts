import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SigninComponent } from './admin/signin/signin.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; 
import { ToastrModule } from 'ngx-toastr';
import { ProjectListComponent } from './project/project-list/project-list.component';
import { ProjectAddComponent } from './project/project-add/project-add.component';
import { EmployeeListComponent } from './employee/employee-list/employee-list.component';
import { EmployeeAddComponent } from './employee/employee-add/employee-add.component';
import { AddCustomerComponent } from './customer/add-customer/add-customer.component';
import { SingleProjectComponent } from './project/single-project/single-project.component';

@NgModule({
  declarations: [
    AppComponent,
    SigninComponent,
    ProjectListComponent,
    ProjectAddComponent,
    EmployeeListComponent,
    EmployeeAddComponent,
    AddCustomerComponent,
    SingleProjectComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdminService } from '../admin.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  userName = ''
  password = ''

  constructor(
    private router: Router,
    private toastr: ToastrService,
    private service: AdminService) { }

  ngOnInit(): void {
  }

  onSignin() {
    if (this.userName.length == 0) {
      this.toastr.warning('please enter user name')
    } else if (this.password.length == 0) {
      this.toastr.warning('please enter password')
    } else {
      this.service
        .signin(this.userName, this.password)
        .subscribe( data => {
            console.log(data)
            this.toastr.success('welcome')
            sessionStorage['userName'] = data['userName'] 
            sessionStorage['token'] = data['jwt']
            this.router.navigate(['/project-list'])
           
          }, error => {
            console.log(error)
            
            this.toastr.error('invalid credentials !')
          });
    }
  }

}


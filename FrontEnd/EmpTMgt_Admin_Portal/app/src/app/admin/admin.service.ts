import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class AdminService {
  private url = 'http://localhost:8081/project'
 
  constructor(
    private router: Router,
    private http: HttpClient) {}


    signin(userName: string, password: string) {
      const body = {
        userName: userName,
        password: password
      }
  
      return this.http.post(this.url + "/authenticate", body)
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

      // check if user is already logged in
      if (!sessionStorage['token']) {
        // user is not yet logged in
  
        // force user to login
        this.router.navigate(['/signin'])
  
        return false
      }
  
      // user is already logged in
      return true
    }


}

import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';

import { EmployeeAddComponent } from '../employee-add/employee-add.component';
import { Employees } from '../employee';


@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {
  
  employees: Employees[];
      empId =1
      firstName ='' 
      lastName =''
      email =''
      address =''
      birthDate ='' 
      contInfo =''
      gender =''
      department =''
      designation =''
      //selectedProjects =1

  constructor(
    private modalService: NgbModal,
    private toastr: ToastrService,
    private service: EmployeeService,
    private router: Router) { }

  ngOnInit(): void {
    this.service.findAll().subscribe(data => {
      //alert(data);
      console.log(data);
      this.employees = data;
      //alert(this.employees);
     
    });
  }
 
  onAdd() {
    const modalRef = this.modalService.open(EmployeeAddComponent, {size: 'lg'})
    modalRef.result.finally(() => {
      this.service.findAll();
    })
  }
}
 

    


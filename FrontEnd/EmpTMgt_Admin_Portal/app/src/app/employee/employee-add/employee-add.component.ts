import { EmployeeService } from './../employee.service';
import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-employee-add',
  templateUrl: './employee-add.component.html',
  styleUrls: ['./employee-add.component.css']
})
export class EmployeeAddComponent implements OnInit {

  firstName = ''
  lastName = ''
  email = ''
  address = ''
  birthDate = ''
  contInfo =''
  gender =''
  department =''
  designation =''
  //selectedProject=1
  //photo = undefined

  projects = []


  constructor(private modal: NgbActiveModal,
    private employeeService: EmployeeService,
    //private projectService: ProjectService,
    private toastr: ToastrService,
    ) { }

  ngOnInit(): void {
    
  }
  

 /* loadProjects() {
    this.projectService
      .getProjects()
      .subscribe(data => {
        if (data['status'] == 'success') {
          this.projects = data['data']
        } else {
          this.toastr.error(data['error'])
        }
      })
  }*/


  onAdd() {
    if (this.firstName.length == 0) {
      this.toastr.warning('please enter first name')
    } else if (this.lastName.length == 0) {
      this.toastr.warning('please enter first name')
    } else if (this.email.length == 0) {
      this.toastr.warning('please enter email')
    } else if (this.address.length == 0) {
      this.toastr.warning('please enter address')
    } else if (this.birthDate.length == 0) {
      this.toastr.warning('please enter birthdeleteEmployee date')
    } else if (this.contInfo.length == 0) {
      this.toastr.warning('please enter contact details')
    } else if (this.gender.length == 0) {
      this.toastr.warning('please enter gender')
    }  else if (this.department.length == 0) {
      this.toastr.warning('please enter department')
    } else if (this.designation.length == 0) {
      this.toastr.warning('please enter designation')
    }    
    else {
      this.employeeService
        .addEmployee(this.firstName, this.lastName, this.email, this.address, this.birthDate, this.contInfo ,this.gender ,this.department ,this.designation)
        .subscribe(data => {
          console.log(data)
            this.modal.dismiss('ok')
          }, error=> {
            console.log(error)
            this.toastr.error('failed to add..!')
          });
        
    }
  }

  onCancel() {
    this.modal.dismiss('cancel')
  }
}
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Employees } from './employee';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  private url = 'http://localhost:8081/project/admin'

  constructor(private http: HttpClient) { }

  addEmployee(firstName : string, lastName : string, email : string , address : string , birthDate :string, contInfo :string ,gender :string ,department : string ,designation :string ) {
    
    const body = {
      firstName :firstName, 
      lastName :lastName,
      email :email, 
      address :address, 
      birthDate :birthDate, 
      contInfo :contInfo ,
      gender :gender ,
      department :department ,
      designation :designation ,
      //selectedProjects:selectedProjects      
    }

    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    }
    return this.http.post(this.url+'/add-employee', body)
  }

  getEmployeesList(): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    }
    return this.http.get(this.url+'/show-employees');
  }
  public findAll(): Observable<Employees[]> {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    }
    return this.http.get<Employees[]>(this.url+'/show-employees');
  }

  // getEmployee() {

  //   const httpOptions = {
  //     headers: new HttpHeaders({
  //       token: sessionStorage['token']
  //     })
  //   }

  //   return this.http.get(this.url+"/show-employees", httpOptions)
  // }

  deleteEmployee(id: number) {

    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    }
    return this.http.delete(this.url + "/" + id)
  }
}
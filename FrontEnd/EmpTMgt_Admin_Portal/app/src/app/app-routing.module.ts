import { SingleProjectComponent } from './project/single-project/single-project.component';
import { EmployeeListComponent } from './employee/employee-list/employee-list.component';
import { ProjectListComponent } from './project/project-list/project-list.component';
import { ProjectAddComponent } from './project/project-add/project-add.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SigninComponent } from './admin/signin/signin.component';
import { EmployeeAddComponent } from './employee/employee-add/employee-add.component';

const routes: Routes = [
  { path: '', redirectTo: '/signin', pathMatch: 'full' },
  { path: 'signin', component: SigninComponent },
  { path:'project-add', component:ProjectAddComponent },
  { path: 'project-list',component:ProjectListComponent},
  {path:'employee-list', component:EmployeeListComponent},
  {path:'employee-add', component:EmployeeAddComponent},
  {path:'single-project/:id',component:SingleProjectComponent}, 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

package com.app.pojos;

import java.sql.Time;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.*;
@Entity
@Table(name = "employees")
public class Employees {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer empId;
	@Column(length = 20)
	private String firstName;
	@Column(length = 20)
	private String lastName;
	@Column(length = 30,unique = true)	
	private String email;
	@Column(length = 20)
	private String address;
	private LocalDate birthDate;
	@Column(length = 20)
	private String contactInfo;
	@Column(length = 10)
	private String gender;
	@Column(length = 15)
	private String department;
	private String designation;
	@Column(length = 50, columnDefinition="")
	private byte[] photo;
	
	 @ManyToMany(mappedBy = "employees")
	private List<Project> workingProject=new ArrayList<>();
	  
	public Employees() {
	
	}

	public Employees(Integer empId, String firstName, String laststName,String email, String address, LocalDate birthDate,
			String contInfo, String gender, String department,  String designation,byte[] photo) {
		this.empId = empId;
		this.firstName = firstName;
		this.lastName = laststName;
		this.email = email;
		this.address = address;
		this.birthDate = birthDate;
		this.contactInfo = contInfo;
		this.gender = gender;
		this.department = department;
		this.designation = designation;
		this.photo = photo;
	
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String laststName) {
		this.lastName = laststName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public String getContactInfo() {
		return contactInfo;
	}

	public void setContactInfo(String contInfo) {
		this.contactInfo = contInfo;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String position) {
		this.department = position;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}


	public byte[] getPhoto() {
		return photo;
	}

	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}

	public List<Project> getWorkingProject() {
		
		return workingProject;
	}
	
	public void setWorkingProject(List<Project> workingProject) {
		this.workingProject=workingProject;
		
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employees other = (Employees) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		return true;
	}
	
	
	@Override
	public String toString() {
		return "Employees [empId=" + empId + ", firstName=" + firstName + ", laststName=" + lastName + ", email="
				+ email + ", address=" + address + ", birthDate=" + birthDate + ", contInfo=" + contactInfo + ", gender="
				+ gender + ", position=" + department + ", dailyRate=" + ", schedule=" + designation
				+ ", allocatedLeave=" +  ", photo=" + photo +  "]";
	}
}

package com.app.pojos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "taskbar")
public class TaskBar {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(length = 20)
	private String gatherInformation;
	@Column(length = 20)
	private String Planning;
	@Column(length = 20)
	private String Design;
	@Column(length = 20)
	private String Development;
	@Column(length = 20)
	private String  ContentWriting;
	@Column(length = 20)
	private String Testing;
	@Column(length = 20)
	private String  Maintenance;
	@Column(length = 20)
	private String Deployment;
	
	@OneToOne 
	@JoinColumn(name="project_id")
	private Project projectName;
	
	
	private int completionStatus;
	
	public String getGatherInformation() {
		return gatherInformation;
	}
	public void setGatherInformation(String gatherInformation) {
		this.gatherInformation = gatherInformation;
	}
	public String getPlanning() {
		return Planning;
	}
	public void setPlanning(String planning) {
		Planning = planning;
	}
	public String getDesign() {
		return Design;
	}
	public void setDesign(String design) {
		Design = design;
	}
	public String getDevelopment() {
		return Development;
	}
	public void setDevelopment(String development) {
		Development = development;
	}
	public String getContentWriting() {
		return ContentWriting;
	}
	public void setContentWriting(String contentWriting) {
		ContentWriting = contentWriting;
	}
	public String getTesting() {
		return Testing;
	}
	public void setTesting(String testing) {
		Testing = testing;
	}
	public String getMaintenance() {
		return Maintenance;
	}
	public void setMaintenance(String maintenance) {
		Maintenance = maintenance;
	}
	public String getDeployment() {
		return Deployment;
	}
	public void setDeployment(String deployment) {
		Deployment = deployment;
	}
	
	
	
	
}

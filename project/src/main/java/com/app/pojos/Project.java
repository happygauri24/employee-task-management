package com.app.pojos;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
@Entity
@Table(name = "projects_tbl")
@JsonInclude(Include.NON_DEFAULT)
public class Project {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	//@JsonProperty("id")
	@Column(name = "project_id")
	private Integer projectId;
	@Column(length = 20)
	//@JsonProperty("projectName")
	private String projectName;
	//private String TeamleadName;
	@Column(length = 50)
	private String projectDesc;
	private LocalDate startDate;
	private LocalDate endDate;
	@JsonProperty("status")
	private String status;
	
	@ManyToMany(cascade = {CascadeType.PERSIST,CascadeType.MERGE},fetch = FetchType.EAGER)
	@JoinTable(name ="projects_employees",joinColumns = @JoinColumn(name="p_id"),inverseJoinColumns =@JoinColumn(name="employee_id") )
	private List<Employees> employees=new ArrayList<>();
	
	
	@OneToOne(mappedBy = "projectName",cascade = CascadeType.ALL)	
	private Customer customerDetails;
	
	@OneToOne(mappedBy = "projectName",cascade = CascadeType.ALL)	
	private TaskBar progress;
		
	public Project() {
		
	}
	
	public Project(String projectName, String projectDesc, LocalDate startDate, LocalDate endDate, String status) {
		super();
		this.projectName = projectName;
		this.projectDesc = projectDesc;
		this.startDate = startDate;
		this.endDate = endDate;
		this.status=status;
	}

	public Integer getProjectId() {
		return projectId;
	}
	public void setProjectId(Integer projecttId) {
		this.projectId = projecttId;
	}
	
	public String getProjectDesc() {
		return projectDesc;
	}
	public void setProjectDesc(String projecttDesc) {
		this.projectDesc = projecttDesc;
	}
	
	public LocalDate getstartDate() {
		return startDate;
	}
	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}
	public LocalDate getEndDate() {
		return endDate;
	}
	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}
	
	
	public List<Employees> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employees> employees) {
		this.employees = employees;
	}

	public TaskBar getProgress() {
		return progress;
	}

	public void setProgress(TaskBar progress) {
		this.progress = progress;
	}

	public void addEmployee(Employees e)
	{
		employees.add(e);
		e.getWorkingProject().add(this);
		
	}
	
	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public void removeEmployee(Employees e)
	{
		employees.remove(e);
		e.getWorkingProject().remove(this);
		
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Customer getCustomerDetails() {
		return customerDetails;
	}

	public void setCustomerDetails(Customer customerDetails) {
		this.customerDetails = customerDetails;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((projectName == null) ? 0 : projectName.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Project other = (Project) obj;
		if (projectName == null) {
			if (other.projectName != null)
				return false;
		} else if (!projectName.equals(other.projectName))
			return false;
		return true;
	}
	

	@Override
	public String toString() {
		return "Product [projectId=" + projectId + ", name=" + projectName + "," + ", projecttDesc="
				+ projectDesc + ", endDate=" + endDate + ", startDate" + startDate + "]";
	}
	
	
}

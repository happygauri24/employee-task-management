package com.app.pojos;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
@Entity
@Table(name = "customers_tbl")
@JsonInclude(Include.NON_DEFAULT)
public class Customer {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonProperty("id")
	private Integer customerId;
	@Column(length = 20)
	private String customerName;
	@Column(length = 50)
	private String requirements;
	
	@OneToOne 
	@JoinColumn(name="project_id")
	private Project projectName;
	
	public Customer() {
	
	}
	public Customer(String customerName, String requirements) {
		super();
		this.customerName = customerName;
		this.requirements = requirements;
	}


	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public Project getProjectName() {
		return projectName;
	}
	public void setProjectName(Project projectName) {
		this.projectName = projectName;
	}
	public String getRequirements() {
		return requirements;
	}
	public void setRequirements(String requirements) {
		this.requirements = requirements;
	}
	
	@Override
	public String toString() {
		return "Customer [customerId=" + customerId + ", projectName=" + projectName + ", customerName=" + customerName
				+ ", requirements=" + requirements + "]";
	}
	
}

package com.app.controller;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpSession;
import javax.websocket.server.PathParam;
import org.springframework.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.ResponseEntity;
import com.app.pojos.Customer;
import com.app.pojos.Employees;
import com.app.pojos.Project;
import com.app.service.ICustomerJPAService;
import com.app.service.ICustomerService;
import com.app.service.IEmployeeJpaService;
import com.app.service.IEmployeesService;
import com.app.service.IProjectService;
import com.app.service.ProjectJpaService;

@RestController
@RequestMapping("/admin")
@CrossOrigin
public class AdminController {
	@Autowired
	private IEmployeesService EmployeesService;
	
	@Autowired
	private IProjectService ProjectService;
	
	@Autowired
	private ICustomerService CustomerService;
	
	@Autowired
	private IEmployeeJpaService EmployeesJpaService;
	
	@Autowired
	private ICustomerJPAService customerJService;
	
	@Autowired
	private ProjectJpaService projService;

	public AdminController(){
		
	}
		
	@PostMapping("/add-employee")
	public  ResponseEntity<?>  addEmployee(@RequestBody Employees e )
	{
		String str=EmployeesService.addEmployee(e);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	
	@PostMapping("/add-project")
	public ResponseEntity<?> addProject(@RequestBody Project p) {
		System.out.println("req received");
	ProjectService.addProject(p);
	return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@PostMapping("/add-customer")
	public String addCustomer(@RequestBody Customer c) {
		try {

			String msg=CustomerService.addCustomer(c);

			return "redirect:/admin/add-customer";
			} catch (RuntimeException e) {
			System.out.println("err in process login form " + e);
			return "/admin";
			} 
	}
	
	
	@RequestMapping(value = "/assign-project/{id}", method = RequestMethod.POST)
	@ResponseBody
	public String assignProjectToEmployee(@RequestBody List<Project> p, @PathVariable int id) {
		EmployeesJpaService.updateProjectDetails(p, id);
		return "redirect:/admin/assign-project";
	}
	
	
	@GetMapping("/show-customers")
	public ResponseEntity<?> showCustomers() {
		System.out.println("in fetch all products");
		List<Customer> customers = CustomerService.showCustomers();
		
		if (customers.isEmpty())
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		// non empty list
		return new ResponseEntity<>(customers, HttpStatus.OK);
	}

	
	@GetMapping("/show-employees")
	public ResponseEntity<?> fetchAllEmployees() {
		System.out.println("in fetch all products");
		List<Employees> employees = EmployeesService.showEmployees();
		
		if (employees.isEmpty())
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		// non empty list
		return new ResponseEntity<>(employees, HttpStatus.OK);
	}
	
	
	//@PostMapping("/assign-customer/{cid}")
    @RequestMapping(value = "/assign-customer/{id}", method = RequestMethod.POST)
    @ResponseBody
	public String assignCustomer(@RequestBody Project p,@PathVariable int id)
	{
    	customerJService.updateCustomerDetails(p,id);
		return "redirect:/admin/assign-customer";
	}

	
    @GetMapping("/show-projects")
	public ResponseEntity<?> fetchAllProjects() {
		System.out.println("in fetch all projects");
		List<Project> projects = ProjectService.showProject();
		if (projects.isEmpty())
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		
		return new ResponseEntity<List<Project>>(projects, HttpStatus.OK);
	}

    @GetMapping("/select-project/{pid}")
   	public Project findProjectById(@PathVariable int pid) {
   		System.out.println("in fetch all projects");
   		Project projects = projService.findProjectById(pid);
   		return projects;
    }
    
    
}

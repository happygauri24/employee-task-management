package com.app.controller;

import org.springframework.http.HttpStatus;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.ResponseEntity;
import com.app.pojos.Employees;
import com.app.pojos.Registration;
import com.app.pojos.Role;
import com.app.service.EmployeesService;
import com.app.service.IEmployeesService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class EmployeesController {
	
	@Autowired
	private IEmployeesService EmployeesService;
	
	public EmployeesController() {
		System.out.println("in ctor of " + getClass().getName());

	}

	@GetMapping("/login")
	public String showLoginForm() {
		System.out.println("in show login form");
		return "/employee/login";
	}

	
	@PostMapping("/login")
	public ResponseEntity<?> processLoginForm(@RequestBody Registration r) {
		System.out.println("in process login form " + r.getEmail() + " " + r.getPassword());
		
			Registration registeredEmployee = EmployeesService.authenticateEmp(r);
				return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@PostMapping("/signup")
	public String registerEmployee(@RequestBody Registration r) {
		
		String str=EmployeesService.registerEmployee(r);	
		
		return str;
		
	}
	
	
	
}


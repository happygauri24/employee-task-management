package com.app.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.app.pojos.Project;
import com.app.service.CustomerJPAService;
import com.app.service.EmployeeServiceJpa;
import com.app.service.ProjectJpaService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class ProjectController {

	@Autowired
	private ProjectJpaService projectService;
	@Autowired
	private CustomerJPAService custService;
	@Autowired
	private EmployeeServiceJpa empService;
	

	@PostMapping("add-project")
	public ResponseEntity<?> addProject(@RequestBody Project project) {
		empService.addEmployee(project.getEmployees());
		custService.addCustomer(project.getCustomerDetails());
		
		if(projectService.addProject(project) != null) {
			return new ResponseEntity<>(true, HttpStatus.OK);
		}
		return new ResponseEntity<>(false, HttpStatus.EXPECTATION_FAILED);
	}
	
	
	@GetMapping("all-projects")
	public ResponseEntity<?> getAllProjects() {
		System.out.println("in get all projects  ");
		try {
			return ResponseEntity.ok(projectService.getProjects());
		} catch (RuntimeException e) {
			System.out.println("err in get all songs " + e);
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

}

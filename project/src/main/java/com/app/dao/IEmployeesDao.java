package com.app.dao;


import java.util.List;
import java.util.Optional;



import com.app.pojos.Employees;
import com.app.pojos.Registration;

public interface IEmployeesDao
{
	String registerEmployee(Registration r);
	
	Registration authenticateEmployee(Registration r);
	
	String addEmployee(Employees e);
	
	//Optional<Employees> findByName(String productName);
	List<Employees> showEmployees(); 
}

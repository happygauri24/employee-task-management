package com.app.dao;

import java.util.List;

import com.app.pojos.Customer;
import com.app.pojos.Project;

public interface ICustomerDao {

	String addCustomer(Customer c);
	List<Customer> showCustomers();
	String assignProjectToCustomer(Project p,int id);
}
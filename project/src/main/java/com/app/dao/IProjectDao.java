package com.app.dao;

import java.util.List;
import java.util.Set;

import com.app.pojos.Project;

public interface IProjectDao {

	String addProject(Project p);
	String removeProject(Project p);
	//String assignProjectToEmployee(String p, int id);
	List<Project> showProject();
	String assignProjectToEmployee(List<Project> p, int id); 
}

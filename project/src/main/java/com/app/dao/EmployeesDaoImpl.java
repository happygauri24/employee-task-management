package com.app.dao;

import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import com.app.pojos.Employees;
import com.app.pojos.Project;
import com.app.pojos.Registration;
import com.app.pojos.Role;
import com.app.service.EmployeeServiceJpa;

@Repository
public class EmployeesDaoImpl  implements IEmployeesDao{
	
	@Autowired
	EmployeeServiceJpa empService;
	
	@PersistenceContext
	private EntityManager manager;
	@Override
	public String registerEmployee(Registration r) {
		//Registration reg = null;
	//String jpql = "insert into Registration (email, password) values (:em, :pass)";
	//reg = manager.createQuery(jpql, Registration.class).setParameter("em", r.getEmail()).setParameter("pass", r.getPassword()).getSingleResult();
		manager.createNativeQuery("INSERT INTO registration_tbl (email,password) VALUES (?,?)")  // need to use this querry.
		.setParameter(1, r.getEmail())
		.setParameter(2, r.getPassword())
		.executeUpdate(); 		
			return "successfully signed up";
	}

	@Override
	public Registration authenticateEmployee(Registration r) {
		Registration e = null;
		String jpql = "select r from Registration r where r.email=:em and r.password=:pass";

		e = manager.createQuery(jpql, Registration.class).setParameter("em",r.getEmail())
				.setParameter("pass", r.getPassword()).getSingleResult();
		System.out.println(e.toString());
		if(r.getEmail().equals(e.getEmail()) && r.getPassword().equals(e.getPassword()))
		{
				return e;
		}

		return null;	
	}

	@Override
	public String addEmployee(Employees e) {		//	1		2		3		4		5			6			7
manager.createNativeQuery("INSERT INTO employees (first_name,last_name,email,address,birth_date,contact_info,gender,"
		+ "department,designation) VALUES (?,?,?,?,?,?,?,?,?)")  // need to use this querry.
		.setParameter(1, e.getFirstName())
		.setParameter(2, e.getLastName()).setParameter(3, e.getEmail()).setParameter(4, e.getAddress()).setParameter(5, e.getBirthDate())
		.setParameter(6, e.getContactInfo()).setParameter(7, e.getGender()).setParameter(8, e.getDepartment()).setParameter(9, e.getDesignation())
		.executeUpdate(); 		
			return "successfully signed up";
	
	}

	@Override
	public List<Employees> showEmployees() {
		String jpql = "select e from Employees e";
		return manager.createQuery(jpql, Employees.class).getResultList(); 
	}

}

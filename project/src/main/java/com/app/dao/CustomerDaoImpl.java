package com.app.dao;

import java.util.List;
import javax.persistence.Query;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.app.pojos.Customer;
import com.app.pojos.Project;
@Repository
public class CustomerDaoImpl implements ICustomerDao {

	@PersistenceContext
	private EntityManager manager;
	@Override
	public String addCustomer(Customer c) {
		
	manager.createNativeQuery("INSERT INTO customers_tbl (customer_name,requirements) VALUES (?,?)") 
		.setParameter(1, c.getCustomerName())
		.setParameter(2, c.getRequirements())
		.executeUpdate(); 
	return "Successfully Customer added";
		
	}
	
	@Override
	public List<Customer> showCustomers() {
		Query q= manager.createNativeQuery("select * from customers_tbl");
		List<Customer> customers = q.getResultList();
		return customers;
	}

	@Override
	public String assignProjectToCustomer(Project p,int id) {
		String msg="Assigning Customer failed";
		Customer c= null;
		try {
		String jpql="update Customer c set c.setProjectName = :pid where c.customerId = :id";
				int change=manager.createQuery(jpql).setParameter("id",id).setParameter("pid",p.getProjectId()).executeUpdate();
				c.setProjectName(p);
				System.out.println(p.toString());
				msg="Customer assigned to project.."+change;
		}
		catch(RuntimeException e)
		{
			throw e;
		}
		return msg;
		
	}



}
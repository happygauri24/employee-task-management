package com.app.dao;

import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.app.pojos.Employees;
import com.app.pojos.Project;
import com.app.pojos.Registration;
import com.app.service.EmployeeServiceJpa;

@Repository
public class ProjectDaoImpl implements IProjectDao {

	@Autowired
	EmployeeServiceJpa empService;
	
	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public String addProject(Project p) {
manager.createNativeQuery("INSERT INTO projects_tbl (project_name,project_desc,start_date,end_date,status)"+ "VALUES (?,?,?,?,?)")
				.setParameter(1,p.getProjectName())
				.setParameter(2,p.getProjectDesc()).setParameter(3, p.getstartDate()).setParameter(4,p.getEndDate()).
				setParameter(5, p.getStatus()).executeUpdate(); 		
					return "successfully signed up";
		
	}

	@Override
	public String removeProject(Project p) {
		// TODO Auto-generated method stub
		return null;
	}
//*****************************************************************need to verify according to UI **********************************************
	/*@Override
	public String assignProjectToEmployee(String email, int id) {
		String mesg = "assigning project failed...";
		Employees e = null;
		Project p = null;
		try {
			String jpql = "select e from Employees e where e.email=:email";
			String jpql1="select p from Project p where p.projectId=:id";
			e = manager.createQuery(jpql, Employees.class).setParameter("email",e.getEmail()).getSingleResult();
			p=manager.createQuery(jpql1, Project.class).setParameter("id",p.getProductId()).getSingleResult();
			e.setWorkingProject(p);
			System.out.println(e.toString());
			mesg = "Project linked to employee : " + e.getFirstName();
		} catch (RuntimeException exc) {
			throw exc;
		}
		return mesg;
			
	}*/
	
	
	@Override
	public String assignProjectToEmployee(List<Project> p, int id) {
		String mesg = "assigning project failed...";
		Employees e = null;
		try {
			String jpql = "select e from Employees e where e.empId=:id";
			e = manager.createQuery(jpql, Employees.class).setParameter("id",id).getSingleResult();
			e.setWorkingProject(p);
			System.out.println(e.toString());
		} catch (RuntimeException exc) {
			throw exc;
		}
		return mesg;
			
	}
	
	/*@Override
	public Product updateProductDetails(Product p) {
		// chk if product exists
		Optional<Product> optional = productRepo.findById(p.getProductId());
		if (optional.isPresent())
			return productRepo.save(p); // update
		// if product is not found : throw custom exception
		throw new ProductNotFoundException("Product Not Found : Invalid Product id " + p.getProductId());

	}*/
	
	@Override
	public List<Project> showProject() {
		String jpql = "select p from Project p";
		return manager.createQuery(jpql, Project.class).getResultList(); 
	}
}

package com.app.dao;
import org.springframework.data.jpa.repository.JpaRepository;

import com.app.pojos.Employees;

public interface EmployeeRepository extends JpaRepository <Employees,Integer>{

}

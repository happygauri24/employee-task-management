package com.app.service;

import java.util.List;
import java.util.Set;

import com.app.pojos.Employees;
import com.app.pojos.Project;

public interface IEmployeeJpaService {
	Employees updateProjectDetails(List<Project> p, int id);
}

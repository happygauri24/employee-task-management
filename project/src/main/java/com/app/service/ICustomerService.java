package com.app.service;

import java.util.List;

import com.app.pojos.Customer;


public interface ICustomerService {

	public String addCustomer(Customer c);
	public List<Customer> showCustomers(); 
}
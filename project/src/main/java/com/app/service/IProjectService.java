package com.app.service;

import java.util.List;
import java.util.Set;

import com.app.pojos.Project;

public interface IProjectService {
	String addProject(Project p);
	String removeProject(Project p);
	 List<Project> showProject();
	String assignProjectToEmployee(List<Project> p, int id);
}

package com.app.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.dao.CustomerRepository;
import com.app.dao.ProjectRepository;
import com.app.pojos.Customer;
import com.app.pojos.Project;

@Service // mandatory
@Transactional
public class CustomerJPAService implements ICustomerJPAService {

	@Autowired
	private CustomerRepository customerRepo;
	
	@Autowired
	private ProjectRepository projectRepo;
	@Override
	public Customer updateCustomerDetails(Project p,int id) {
		Optional<Customer> optional = customerRepo.findById(id);
		if (optional.isPresent())
		{
			Customer c=optional.get();
			//c.setProjectName(p);
			projectRepo.save(p);
			return customerRepo.save(c); 
		}
		

		return null;
	}

	public Customer addCustomer(Customer cust)
	{
		return customerRepo.save(cust);
	}
}

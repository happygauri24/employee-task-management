package com.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.ICustomerDao;
import com.app.pojos.Customer;

@Service 
@Transactional 
public class CustomerService implements ICustomerService {

	@Autowired
	private ICustomerDao cDao;
	@Override
	public String addCustomer(Customer c) {
		cDao.addCustomer(c);
		return null;
	}

	@Override
	public List<Customer> showCustomers() {
		return cDao.showCustomers();
	}

}
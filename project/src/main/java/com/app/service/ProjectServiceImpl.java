package com.app.service;

import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.app.dao.IEmployeesDao;
import com.app.dao.IProjectDao;
import com.app.pojos.Project;


@Service
@Transactional
public class ProjectServiceImpl implements IProjectService {

	@Autowired
	private IProjectDao pDao;
	
	@Override
	public String addProject(Project p) {
		return pDao.addProject(p);
		
	}

	@Override
	public String removeProject(Project p) {
		// TODO Auto-generated method stub
		return null;
	}

	/*@Override
	public String assignProjectToEmployee(String p, int id) {
		
		return pDao.assignProjectToEmployee(p, id);
	}*/
	
	@Override
	public String assignProjectToEmployee(List<Project> p, int id) {
		
		return pDao.assignProjectToEmployee(p, id);
	}
	

	@Override
	public List<Project> showProject() {
		
		return pDao.showProject();
	}

	

}

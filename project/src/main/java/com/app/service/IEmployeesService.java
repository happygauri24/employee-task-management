package com.app.service;

import java.util.List;

import com.app.pojos.Employees;
import com.app.pojos.Registration;

public interface IEmployeesService {

	public Registration authenticateEmp(Registration r);
	
	public String registerEmployee(Registration r);
	
	public String addEmployee(Employees e);
	
	List<Employees> showEmployees();
	
}

package com.app.service;

import java.util.List;

import com.app.pojos.Employees;
import com.app.pojos.Project;

public interface IProjectJpaService {

	Project updateProductDetails(Project p, int id);
	public Project addProject(Project project);
	List<Project> getProjects();

}

package com.app.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.app.dao.IEmployeesDao;
import com.app.pojos.Employees;
import com.app.pojos.Registration;

@Service
@Transactional
public class EmployeesService implements IEmployeesService {

	@Autowired
	private IEmployeesDao eDao;
	
	@Override
	public Registration authenticateEmp(Registration r) {
		return eDao.authenticateEmployee(r);
	}
	@Override
	public String registerEmployee(Registration r) {
		return eDao.registerEmployee(r);
		
	}
	@Override
	public String addEmployee(Employees e) {
	
		String str=eDao.addEmployee(e);
		return null;
	}
	

	@Override
	public List<Employees> showEmployees() {
		return eDao.showEmployees();
	}

}

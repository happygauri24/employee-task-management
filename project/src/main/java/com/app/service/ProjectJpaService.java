package com.app.service;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.EmployeeRepository;
import com.app.dao.ProjectRepository;
import com.app.pojos.Employees;
import com.app.pojos.Project;



@Service 
@Transactional
public class ProjectJpaService implements IProjectJpaService {
	
	@Autowired
	private ProjectRepository projectRepo;
	
	@Override
	public Project updateProductDetails(Project p, int id) {
		
		return null;
	}
	
	public Project findProjectById(int id) {
		Optional<Project> optional = projectRepo.findById(id);
		Project e=null;
		if (optional.isPresent())
		{
			e=optional.get();
		}
		return e;
	}
	
	public Project addProject(Project project) {
		return projectRepo.save(project);
	}

	@Override
	public List<Project> getProjects() {
		return projectRepo.findAll();
	}
	

}

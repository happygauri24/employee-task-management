package com.app.service;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.app.dao.EmployeeRepository;
import com.app.dao.ProjectRepository;
import com.app.pojos.Employees;
import com.app.pojos.Project;

@Service 
@Transactional
public class EmployeeServiceJpa implements IEmployeeJpaService {

	@Autowired
	private EmployeeRepository empRepo;
	
	@Autowired
	private ProjectRepository projRepo;
	
	
	@Override
	public Employees updateProjectDetails(List<Project> p,int id) {
		Optional<Employees> optional = empRepo.findById(id);
		if (optional.isPresent())
		{
			Employees e=optional.get();
			e.setWorkingProject(p);
			projRepo.saveAll(p);
			return empRepo.save(e); 
		}
			
		return null;
	}

	
	public Employees findEmployeeById(int id) {
		Optional<Employees> optional = empRepo.findById(id);
		Employees e=null;
		if (optional.isPresent())
		{
			e=optional.get();
		}
		return e;
	}
	
	public List<Employees> addEmployee(List<Employees> emp) {
		return empRepo.saveAll(emp);
	}
}
